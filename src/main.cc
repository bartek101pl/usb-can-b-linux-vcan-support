
#include <fcntl.h>
#include <spdlog/spdlog.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <array>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <memory>
#include <sstream>
#include <thread>

#include "can_server.h"
#include "lib/controlcan.h"
#include "unistd.h"

VCI_BOARD_INFO pInfo;
int count = 0;
VCI_BOARD_INFO pInfo1[50];
int num = 0;

void RxLoop(std::shared_ptr<canbus::server::CanServer> can1,
            std::shared_ptr<canbus::server::CanServer> can2) {
  int reclen = 0;
  VCI_CAN_OBJ rec[3000];
  int i, j;
  int ind = 0;

  while (true) {
    if ((reclen = VCI_Receive(VCI_USBCAN2, 0, ind, rec, 3000, 100)) > 0) {
      for (j = 0; j < reclen; j++) {
        if (ind == 0) {
          std::array<uint8_t, 8> payload;
          std::memcpy(&payload, rec[j].Data, rec[j].DataLen);
          can1->Send(rec[j].ID, payload, rec[j].DataLen);
        } else if (ind == 1) {
          std::array<uint8_t, 8> payload;
          std::memcpy(&payload, rec[j].Data, rec[j].DataLen);
          can2->Send(rec[j].ID, payload, rec[j].DataLen);
        }
        printf("Index:%04d  ", count);
        count++;
        printf("CAN%d RX ID:0x%08X", ind + 1, rec[j].ID);
        if (rec[j].ExternFlag == 0) printf(" Standard ");
        if (rec[j].ExternFlag == 1) printf(" Extend   ");
        if (rec[j].RemoteFlag == 0) printf(" Data   ");
        if (rec[j].RemoteFlag == 1) printf(" Remote ");
        printf("DLC:0x%02X", rec[j].DataLen);
        printf(" data:0x");
        for (i = 0; i < rec[j].DataLen; i++) {
          printf(" %02X", rec[j].Data[i]);
        }
        printf(" TimeStamp:0x%08X", rec[j].TimeStamp);
        printf("\n");
      }
    }
    ind = !ind;
  }

}

int main(int argc, char const* argv[]) {
  printf(">>this is hello !\r\n"); 

  num = VCI_FindUsbDevice2(pInfo1);

  printf(">>USBCAN DEVICE NUM:");
  printf("%d", num);
  printf(" PCS");
  printf("\n");

  for (int i = 0; i < num; i++) {
    printf("Device:");
    printf("%d", i);
    printf("\n");
    printf(">>Get VCI_ReadBoardInfo success!\n");

    printf(">>Serial_Num:%c", pInfo1[i].str_Serial_Num[0]);
    printf("%c", pInfo1[i].str_Serial_Num[1]);
    printf("%c", pInfo1[i].str_Serial_Num[2]);
    printf("%c", pInfo1[i].str_Serial_Num[3]);
    printf("%c", pInfo1[i].str_Serial_Num[4]);
    printf("%c", pInfo1[i].str_Serial_Num[5]);
    printf("%c", pInfo1[i].str_Serial_Num[6]);
    printf("%c", pInfo1[i].str_Serial_Num[7]);
    printf("%c", pInfo1[i].str_Serial_Num[8]);
    printf("%c", pInfo1[i].str_Serial_Num[9]);
    printf("%c", pInfo1[i].str_Serial_Num[10]);
    printf("%c", pInfo1[i].str_Serial_Num[11]);
    printf("%c", pInfo1[i].str_Serial_Num[12]);
    printf("%c", pInfo1[i].str_Serial_Num[13]);
    printf("%c", pInfo1[i].str_Serial_Num[14]);
    printf("%c", pInfo1[i].str_Serial_Num[15]);
    printf("%c", pInfo1[i].str_Serial_Num[16]);
    printf("%c", pInfo1[i].str_Serial_Num[17]);
    printf("%c", pInfo1[i].str_Serial_Num[18]);
    printf("%c", pInfo1[i].str_Serial_Num[19]);
    printf("\n");

    printf(">>hw_Type:%c", pInfo1[i].str_hw_Type[0]);
    printf("%c", pInfo1[i].str_hw_Type[1]);
    printf("%c", pInfo1[i].str_hw_Type[2]);
    printf("%c", pInfo1[i].str_hw_Type[3]);
    printf("%c", pInfo1[i].str_hw_Type[4]);
    printf("%c", pInfo1[i].str_hw_Type[5]);
    printf("%c", pInfo1[i].str_hw_Type[6]);
    printf("%c", pInfo1[i].str_hw_Type[7]);
    printf("%c", pInfo1[i].str_hw_Type[8]);
    printf("%c", pInfo1[i].str_hw_Type[9]);
    printf("\n");

    printf(">>Firmware Version:V");
    printf("%x", (pInfo1[i].fw_Version & 0xF00) >> 8);
    printf(".");
    printf("%x", (pInfo1[i].fw_Version & 0xF0) >> 4);
    printf("%x", pInfo1[i].fw_Version & 0xF);
    printf("\n");
  }
  printf(">>\n");
  printf(">>\n");
  printf(">>\n");
  if (VCI_OpenDevice(VCI_USBCAN2, 0, 0) == 1) {
    printf(">>open deivce success!\n");
  } else {
    printf(">>open deivce error!\n");
    exit(1);
  }
  if (VCI_ReadBoardInfo(VCI_USBCAN2, 0, &pInfo) == 1) {
    printf(">>Get VCI_ReadBoardInfo success!\n");
    printf(">>Serial_Num:%c", pInfo.str_Serial_Num[0]);
    printf("%c", pInfo.str_Serial_Num[1]);
    printf("%c", pInfo.str_Serial_Num[2]);
    printf("%c", pInfo.str_Serial_Num[3]);
    printf("%c", pInfo.str_Serial_Num[4]);
    printf("%c", pInfo.str_Serial_Num[5]);
    printf("%c", pInfo.str_Serial_Num[6]);
    printf("%c", pInfo.str_Serial_Num[7]);
    printf("%c", pInfo.str_Serial_Num[8]);
    printf("%c", pInfo.str_Serial_Num[9]);
    printf("%c", pInfo.str_Serial_Num[10]);
    printf("%c", pInfo.str_Serial_Num[11]);
    printf("%c", pInfo.str_Serial_Num[12]);
    printf("%c", pInfo.str_Serial_Num[13]);
    printf("%c", pInfo.str_Serial_Num[14]);
    printf("%c", pInfo.str_Serial_Num[15]);
    printf("%c", pInfo.str_Serial_Num[16]);
    printf("%c", pInfo.str_Serial_Num[17]);
    printf("%c", pInfo.str_Serial_Num[18]);
    printf("%c", pInfo.str_Serial_Num[19]);
    printf("\n");

    printf(">>hw_Type:%c", pInfo.str_hw_Type[0]);
    printf("%c", pInfo.str_hw_Type[1]);
    printf("%c", pInfo.str_hw_Type[2]);
    printf("%c", pInfo.str_hw_Type[3]);
    printf("%c", pInfo.str_hw_Type[4]);
    printf("%c", pInfo.str_hw_Type[5]);
    printf("%c", pInfo.str_hw_Type[6]);
    printf("%c", pInfo.str_hw_Type[7]);
    printf("%c", pInfo.str_hw_Type[8]);
    printf("%c", pInfo.str_hw_Type[9]);
    printf("\n");
    {
      std::stringstream stream;
      stream << ">>Firmware Version:V" << std::hex
             << ((pInfo.fw_Version & 0xF00) >> 8) << std::hex
             << ((pInfo.fw_Version & 0xF0) >> 4) << std::hex
             << (pInfo.fw_Version & 0xF);
      std::string result(stream.str());
      spdlog::info(stream.str());
    }
  } else {
    printf(">>Get VCI_ReadBoardInfo error!\n");
    exit(1);
  }

  VCI_INIT_CONFIG config;
  config.AccCode = 0;
  config.AccMask = 0xFFFFFFFF;
  config.Filter = 1;
  config.Timing0 = 0x00;
  config.Timing1 = 0x14;
  config.Mode = 0;

  if (VCI_InitCAN(VCI_USBCAN2, 0, 0, &config) != 1) {
    printf(">>Init CAN1 error\n");
    VCI_CloseDevice(VCI_USBCAN2, 0);
  }

  if (VCI_StartCAN(VCI_USBCAN2, 0, 0) != 1) {
    printf(">>Start CAN1 error\n");
    VCI_CloseDevice(VCI_USBCAN2, 0);
  }

  if (VCI_InitCAN(VCI_USBCAN2, 0, 1, &config) != 1) {
    printf(">>Init can2 error\n");
    VCI_CloseDevice(VCI_USBCAN2, 0);
  }
  if (VCI_StartCAN(VCI_USBCAN2, 0, 1) != 1) {
    printf(">>Start can2 error\n");
    VCI_CloseDevice(VCI_USBCAN2, 0);
  }

  VCI_CAN_OBJ send[1];
  send[0].ID = 0;
  send[0].SendType = 0;
  send[0].RemoteFlag = 0;
  send[0].ExternFlag = 1;
  send[0].DataLen = 8;

  int i = 0;
  for (i = 0; i < send[0].DataLen; i++) {
    send[0].Data[i] = i;
  }

  int m_run0 = 1;
  pthread_t threadid;
  int ret;

  auto can1 = std::make_shared<canbus::server::CanServer>("vcan0");
  auto can2 = std::make_shared<canbus::server::CanServer>("vcan1");
  can1->SetCallback([](const std::uint32_t& id,
                       const std::array<uint8_t, 8> payload,
                       const std::uint8_t& length) {
    VCI_CAN_OBJ send[1];
    send[0].ID = id & 0x1FFFFFFF;
    send[0].SendType = 0;
    send[0].RemoteFlag = 0;
    send[0].ExternFlag = (id & 0x1FFFF800) > 0;
    send[0].DataLen = length;
    std::memcpy(send[0].Data, &payload, length);
    if (VCI_Transmit(VCI_USBCAN2, 0, 0, send, 1) != 1) {
      spdlog::error("[VCI_USBCAN2 CAN1] Send error");
    }
  });
  can2->SetCallback([](const std::uint32_t& id,
                       const std::array<uint8_t, 8> payload,
                       const std::uint8_t& length) {
    VCI_CAN_OBJ send[1];
    send[0].ID = id & 0x1FFFFFFF;
    send[0].SendType = 0;
    send[0].RemoteFlag = 0;
    send[0].ExternFlag = (id & 0x1FFFF800) > 0;
    send[0].DataLen = length;
    std::memcpy(send[0].Data, &payload, length);
    if (VCI_Transmit(VCI_USBCAN2, 0, 1, send, 1) != 1) {
      spdlog::error("[VCI_USBCAN2 CAN2] Send error");
    }
  });
  std::thread start{RxLoop, can1, can2};
  std::thread can1_t{[&, can1]() { can1->Start(); }};
  std::thread can2_t{[&, can2]() { can2->Start(); }};

  start.join();
  VCI_ResetCAN(VCI_USBCAN2, 0, 0);
  std::this_thread::sleep_for(std::chrono::milliseconds{100});
  VCI_ResetCAN(VCI_USBCAN2, 0, 1);
  std::this_thread::sleep_for(std::chrono::milliseconds{100});
  VCI_CloseDevice(VCI_USBCAN2, 0);

  return 0;
}
