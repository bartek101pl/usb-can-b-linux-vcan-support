/**
 * @file can_server.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief this file define CAN bus serwer which receive and send data
 * @version 0.1
 * @date 2023-09-10
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef SRC_CAN_SERVER_H_
#define SRC_CAN_SERVER_H_

#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sdbus-c++/sdbus-c++.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <array>
#include <functional>
#include <memory>
#include <thread>

namespace canbus {
namespace server {
using RXCallback = std::function<void(const std::uint32_t& id,
                                      const std::array<uint8_t, 8> payload,
                                      const std::uint8_t& length)>;
class CanServer {
 private:
  int s;
  struct sockaddr_can addr;
  struct ifreq ifr;
  bool is_initialize = false;
  RXCallback callback_;
  int8_t Init(const std::string& can_device);

 public:
  void Send(const std::uint32_t& id, const std::array<uint8_t, 8> payload,
            const std::uint8_t& length);
  void Start();
  void SetCallback(RXCallback callback);
  CanServer(const std::string& can_device);
  ~CanServer();
};

}  // namespace server
}  // namespace canbus
#endif  // SRC_CAN_SERVER_H_
