/**
 * @file can_server.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief this file define CAN bus serwer which receive and send data
 * @version 0.1
 * @date 2023-09-10
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "can_server.h"

#include <spdlog/spdlog.h>
#include <string.h>

#include <cstring>

namespace canbus {
namespace server {
int8_t CanServer::Init(const std::string& can_device) {
  if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
    spdlog::error("[SocketCanClient " + can_device +
                  "] An error occurred during creating socket");
    return -1;
  }

  strcpy(ifr.ifr_name, can_device.c_str());
  ioctl(this->s, SIOCGIFINDEX, &ifr);

  memset(&addr, 0, sizeof(addr));
  addr.can_family = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;

  if (bind(this->s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    spdlog::error("[SocketCanClient " + can_device +
                  "] An error occurred during binding");
    return -2;
  }
  spdlog::info("[SocketCanClient " + can_device + "] initialized");
  return 0;
}

void CanServer::Send(const std::uint32_t& id,
                     const std::array<uint8_t, 8> payload,
                     const std::uint8_t& length) {
  try {
    can_frame buffor;

    buffor.can_id = id & 0x1FFFFFFF;
    if ((buffor.can_id & 0x1FFFF800) > 0) {
      buffor.can_id |= CAN_EFF_FLAG;
    }
    buffor.len = length;
    std::memcpy(buffor.data, &payload, length);
    if (write(s, &buffor, sizeof(buffor)) != sizeof(buffor)) {
      spdlog::error("[SocketCanClient] An error occurred during write frame");
    }
  } catch (const std::out_of_range& err) {
  }
}

void CanServer::Start() {
  if (!is_initialize) {
    return;
  }

  can_frame buffor;
  while (true) {
    if (recv(this->s, &buffor, sizeof(buffor), MSG_WAITALL) > 0) {
      if (this->callback_) {
        std::array<uint8_t, 8> payload;
        std::memcpy(&payload, buffor.data, buffor.len);
        callback_(buffor.can_id & 0x1FFFFFFF, payload, buffor.len);
      }
    }
    std::this_thread::yield();
  }
}

void CanServer::SetCallback(RXCallback callback) { this->callback_ = callback; }

CanServer::CanServer(const std::string& can_device) {
  if (this->Init(can_device) == 0) {
    this->is_initialize = true;
  } else {
    this->is_initialize = false;
  }
}

CanServer::~CanServer() {}
}  // namespace server
}  // namespace canbus
