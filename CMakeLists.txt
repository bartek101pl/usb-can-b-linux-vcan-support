cmake_minimum_required(VERSION 3.2)

if(CMAKE_BUILD_TYPE)
	message(STATUS "CMake build type: ${CMAKE_BUILD_TYPE}")
endif()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project(usb-can-b-linux-vcan)

add_subdirectory(src)